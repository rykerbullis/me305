'''@file                mainpage.py
   @brief               Brief doc for mainpage.py
   @details             Detailed doc for mainpage.py 

   @mainpage
   
   @section source      Source Code
                        Available here:
                        https://bitbucket.org/rykerbullis/me305/src/master/Lab4/
   
   @section diagrams4   Task Diagrams and Tuning Plots
                        Task and state diagrams are used to model how different
                        files will interact, perform, and share data. Many of the
                        tasks run simultaneously and must perform their duties 
                        while not intefering with other tasks. Diagrams help to 
                        model the flow of information and structure the code.
   
                        See the following link for pdf documents of the state and task diagrams:
                        https://cpslo-my.sharepoint.com/:f:/g/personal/rbullis_calpoly_edu/EjEavnIYfnRJkHtu7U88-fkB-RLXx9aTk4OoJGsVe4E2eQ?e=o9GsuX

                        The motor controller performed well after tuning, as seen below:
                            
                        \image html Lab4BlockDiagram.png "Block diagram for the motor controller"
                        \image html Lab4Untuned1.png "First iteration of motor tuning"
                        \image html Lab4Untuned2.png "Second iteration of motor tuning"
                        \image html Lab4Untuned3.png "Third iteration of motor tuning"
                        \image html Lab4Untuned4.png "Fourth iteration of motor tuning"
                        \image html Lab4Tuned.png "Tuned motor controller"
                        
                        State Diagrams:
                        \image html Lab4State.jpg "FSM Diagram"
                        \image html Lab4Task.png "Task Diagram"
                        
                        As can be seen from the tuned motor controller plot, the
                        proportional speed controller reaches the final target 
                        velocity and maintains it as directed. 
                        
   @section sec_intro   Introduction
                        Closed loop speed controller for a motor. Uses a
                        proportional controller to set and control the speed 
                        of a motor. Implements motor and encoder tasks, as 
                        well as a user interface.

   @section sec_DRV8847 Motor Driver
                        Driver for the motors.
                        Please see DRV8847.DRV8847 for details.

   @section sec_enc     Encoder Driver
                        Driver for the motor encoders.
                        Please see encoder.Encoder for details.
                        
   @section sec_mot     Motor Task
                        Task which implements the motor driver.
                        Please see task_motor for details.

   @section sec_taskenc Encoder Task
                        Task which implements the encoder driver.
                        Please see task_enc for details.
   
   @section sec_cl      Closed Loop Controller
                        Implements proportional speed control for motor.
                        Please see closedloop for details.

   @section sec_share   Share
                        Allows for sharing of variables between tasks and files.
                        Please see shares.Share for details.
            
   @section sec_con     Task Controller
                        Implements the closed loop motor control in a task.
                        Please see task_controller for details.
                        
   @section sec_safety  Safety Task
                        Implements safety protection for motor faults.
                        Please see task_safety for details.
                        
   @section sec_user    User Task
                        Implements user interface, collecting and printing data.
                        Please see task_user for details.

   @author              Ryker Bullis
   @author              Jack Barone
   
   @date                November 15, 2021
'''