''' @file main.py
    @brief      Main file for BNO055 IMU
    @details   Calls functions from IMU driver to display data
    @author Ryker Bullis
    @author Jack Barone
    @date November 8, 2021
'''
import imu
import pyb
import time
import task_imu



# OPERATING MODES
mode_config = 0x00
mode_NDOF = 0x0C
mode_NDOF_OFF = 0x0B


if __name__ == '__main__':
    
    task1 = task_imu.Task_IMU()
    

    while True:
        try:
            task1.run()
        except KeyboardInterrupt:
            break