import pyb
import time
import struct

while(True):
    try:
        ## Setup
        i2c = pyb.I2C(1,pyb.I2C.MASTER)
        e_angle = bytearray(6)
        cali = bytearray(22)
        
    
        
        i2c.mem_write(0x00,0x28,0x3B)   #Unit selection
        i2c.mem_write(0x0B,0x28,0x3D)   #Operating Mode Selection
        
        
        i2c.mem_read(e_angle,0x28,0x1A)    # Heading LSB
        i2c.mem_read(e_angle,0x28,0x1B)    # Heading MSB
        i2c.mem_read(e_angle,0x28,0x1C)    # Roll LSB
        i2c.mem_read(e_angle,0x28,0x1D)    # Roll MSB
        i2c.mem_read(e_angle,0x28,0x1E)    # Pitch LSB
        i2c.mem_read(e_angle,0x28,0x1C)    # Pitch MSB
        
        i2c.mem_read(cali, 0x28, 0x55)     # ACC_X LSB
        i2c.mem_read(cali, 0x28, 0x56)     # ACC_X MSB
        i2c.mem_read(cali, 0x28, 0x57)     # ACC_Y LSB
        i2c.mem_read(cali, 0x28, 0x58)     # ACC_Y MSB
        i2c.mem_read(cali, 0x28, 0x59)     # ACC_Z LSB
        i2c.mem_read(cali, 0x28, 0x5A)     # ACC_Z MSB
        i2c.mem_read(cali, 0x28, 0x5B)     # MAG_X LSB
        i2c.mem_read(cali, 0x28, 0x5C)     # MAG_X MSB
        i2c.mem_read(cali, 0x28, 0x5D)     # MAG_Y LSB
        i2c.mem_read(cali, 0x28, 0x5E)     # MAG_Y MSB
        i2c.mem_read(cali, 0x28, 0x5F)     # MAG_Z LSB
        i2c.mem_read(cali, 0x28, 0x60)     # MAG_Z MSB
        i2c.mem_read(cali, 0x28, 0x61)     # GYR_X LSB
        i2c.mem_read(cali, 0x28, 0x62)     # GYR_X MSB
        i2c.mem_read(cali, 0x28, 0x63)     # GYR_Y LSB
        i2c.mem_read(cali, 0x28, 0x64)     # GYR_Y MSB
        i2c.mem_read(cali, 0x28, 0x65)     # GYR_Z LSB
        i2c.mem_read(cali, 0x28, 0x66)     # GYR_X MSB
        i2c.mem_read(cali, 0x28, 0x67)     # ACC_RAD LSB
        i2c.mem_read(cali, 0x28, 0x68)     # ACC_RAD MSB
        i2c.mem_read(cali, 0x28, 0x69)     # MAG_RAD LSB
        i2c.mem_read(cali, 0x28, 0x6A)     # MAG_RAD MSB
        
        
        calib_status = i2c.mem_read(8, 0x28, 0x35)
        
        heading = e_angle[0] | e_angle[1] << 8
        roll = e_angle[2] | e_angle[3] << 8
        pitch = e_angle[4] | e_angle[5] << 8
        
        if heading > 32767:
            heading -= 65536
        if roll > 32767:
            roll -= 65536
        if pitch > 32767:
            pitch -= 65536
        
        heading /= 16
        roll /= 16
        pitch /= 16
        #print("Scaled:", (heading, roll, pitch))
        time.sleep(0.1)
        
        cali_signed_ints = struct.unpack('<hhhhhhhhhhh', cali)
        #print('Unpacked: ', cali_signed_ints)
        
    
        
        SYS = calib_status[7] >> 6 | calib_status[6] >> 6
        print('System Calib Status: ', SYS)
        
        GYR = calib_status[5] >> 4 | calib_status[4] >> 4
        print('Gyroscope Calib Status: ', GYR)
        
        ACC = calib_status[3] >> 2 | calib_status[2] >> 2
        print('Accelerometer Calib Status: ', ACC)
        
        MAG = calib_status[1] >> 0| calib_status[0] >> 0
        print('Magnetometer Calib Status: ', MAG)
        
        # e_angles = struct.unpack('<hhh', e_angle)
        # e_vals = tuple(e_angles/16 for eul_int in e_angles)
        
    except KeyboardInterrupt:
        break