'''@file task_user.py
   @brief User interface for encoder and motor operation
   @details    Implements a finite state machine for the user task. Prompts
               the user for inputs and returns data where applicable. 
               Source code available here:
               https://bitbucket.org/rykerbullis/me305/src/master/Lab3/
   @author Ryker Bullis
   @author Jack Barone
   @date November 1, 2021
'''
import utime
import pyb
import array
import sys
import math

S0_INIT             = 0
S1_WAIT             = 1
S2_COLLECT          = 2
S3_COLLECT          = 3
S4_PRINT            = 4
S5_PRINT            = 5

class Task_User:
    ''' @brief User interface for encoder and motor operation
        @details    Implements a finite state machine for the user task. Cycles 
                    through collection, printing, and additional states. 
    '''
    
    def __init__(self,dataPeriod,zero_flag,enc_pos,enc_delta,
                 fault_flag,duty,dbg=True):
        '''@brief              Constructs a user task
           @details            The task is implemented as a finite state
                               machine.
           @param dataPeriod   Period for which data points are collected
           @param zero_flag    Shared object which resets encoder
           @param enc_pos      Shared queue for encoder position
           @param enc_delta    Shared queue for encoder delta
           @param fault_flag   Flag which indicates when a fault has occurred
           @param dbg          Handles serial port error messages
        '''

        ## Set the initial state of the FSM
        self.state = S0_INIT       
        ## The period for checking commands
        self.dataPeriod = dataPeriod        
        ## Serial port to receive keystrokes in PuTTy
        self.ser = pyb.USB_VCP()        
        ## The time of the next FSM run
        self.next_time = utime.ticks_add(utime.ticks_us(), self.dataPeriod)        
        ## Length of time to collect data
        self.dataLength = 30_000_000/dataPeriod        
        ## Create an empty list for the encoder 1 position to record in
        self.dataENC1 = array.array('f',300*[0])
        ## Create an empty list for the encoder 2 position to record in
        self.dataENC2 = array.array('f',300*[0])
        ## Create an empty array for motor 1 velocity
        self.velENC1 = array.array('f',300*[0])
        ## Create an empty array for motor 2 velocity
        self.velENC2 = array.array('f',300*[0])
        ## Create an empty array for the encoder time to record in
        self.dataTime = array.array('f',300*[0])
        ## Define the zero flag for encoder 1
        self.zero_flag = zero_flag
        ## Current position of the encoder
        self.enc_pos = enc_pos
        ## Delta of the encoder
        self.enc_delta = enc_delta
        # Create time step tracker variable
        self.timeEntry = 0
        ## Create fault flag
        self.fault_flag = fault_flag
        ## Duty cycle for motors
        self.duty = duty
        ## Array index for assigning data
        self.arrayIDX = 0
       
        
    def run(self):
        '''@brief Runs an iteration of the FSM
           @details Switches between waiting, collecting data, and printing.
        '''
        
        ## Current time using the utime counter
        current_time = utime.ticks_us()
        
        if (utime.ticks_diff(current_time, self.next_time) >= 0): 
            if self.state == S0_INIT:
                print(' Welcome to the ball balancing program! \n',
                      'Press b to collect 10 seconds of ball data.\n',
                      'Press v to collect 10 seconds of platform data. \n',
                      'Press s to stop data collection early.\n')
                self.transition_to(S1_WAIT)

            elif self.state == S1_WAIT:
                
                ## Create encoder 1 position variable to print
                positionPrint1 = self.enc_pos[0].get()*math.pi/1000
                ## Create encoder 2 position variable to print
                positionPrint2 = self.enc_pos[1].get()*math.pi/1000
                ## Create encoder 1 delta variable to print
                deltaPrint1 = self.enc_delta[0].get()*math.pi/100
                ## Create encoder 2 delta variable to print
                deltaPrint2 = self.enc_delta[1].get()*math.pi/100
                
                if self.ser.any():
                    
                    ## Variable representing keyboard inputs to advance FSM
                    char_in = self.ser.read(1).decode()

                    # Zero the position of encoder 1
                    if(char_in == 'z'):
                        self.zero_flag[0].write(True)
                        print('Encoder 1 has been zeroed.')
                    # Zero the position of encoder 2
                    if(char_in == 'Z'):
                        self.zero_flag[1].write(True)
                        print('Encoder 2 has been zeroed.')

                    # Print the position of encoder 1
                    elif(char_in == 'p'):
                        if(self.enc_pos[0].num_in()>0):
                            print('Encoder 1 Position: {}'.format(positionPrint1))
                    # Print the position of encoder 2
                    elif(char_in == 'P'):
                        if(self.enc_pos[1].num_in()>0):
                            print('Encoder 2 Position: {}'.format(positionPrint2))
                        
                    # Print the delta of encoder 1
                    elif(char_in == 'd'):
                        if(self.enc_delta[0].num_in()>0):
                            print('Encoder 1 Delta: {}'.format(deltaPrint1))
                    # Print the delta of encoder 2
                    elif(char_in == 'D'):
                        if(self.enc_delta[1].num_in()>0):
                            print('Encoder 2 Delta: {}'.format(deltaPrint2))
                            
                    # Collect encoder 1 data for 30 seconds
                    elif(char_in == 'g'):
                        self.transition_to(S2_COLLECT)
                        print('Collecting 30 seconds of encoder 1 data...')
                        
                    # Collect encoder 2 data for 30 seconds
                    elif(char_in == 'G'):
                        self.transition_to(S3_COLLECT)
                        print('Collecting 30 seconds of encoder 2 data...')
                        
                    # Duty cycle input prompt
                    elif(char_in == 'm'):
                        print('Enter motor 1 duty cycle: ')
                        duty1 = self.user_input()
                        self.duty[0].write(duty1)
                    elif(char_in == 'M'):
                        print('Enter motor 2 duty cycle: ')
                        duty2 = self.user_input()
                        self.duty[1].write(duty2)
                        
                    elif(char_in == 'f'):
                        print(self.fault_flag.read())
                                                
            elif self.state == S2_COLLECT:
                self.dataENC1[self.arrayIDX] = self.enc_pos[0].get()*math.pi/1000
                self.velENC1[self.arrayIDX] = self.enc_delta[0].get()*math.pi/100
                self.dataENC2[self.arrayIDX] = self.enc_pos[1].get()*math.pi/1000
                self.velENC2[self.arrayIDX] = self.enc_delta[1].get()*math.pi/100
                self.timeEntry += self.dataPeriod/1_000_000
                self.dataTime[self.arrayIDX] = self.timeEntry
                self.arrayIDX += 1
                if self.arrayIDX >= 300:
                    self.transition_to(S4_PRINT)
                elif self.ser.any():
                    char_in = self.ser.read(1).decode()                    
                    if(char_in == 's' or char_in == 'S'):
                        self.transition_to(S4_PRINT)
                    elif(char_in == 'm'):
                        print('Enter motor 1 duty cycle: ')
                        duty1 = self.user_input()
                        self.duty[0].write(duty1)
                    
            elif self.state == S3_COLLECT:
                self.dataENC1[self.arrayIDX] = self.enc_pos[0].get()*math.pi/1000
                self.velENC1[self.arrayIDX] = self.enc_delta[0].get()*math.pi/100
                self.dataENC2[self.arrayIDX] = self.enc_pos[1].get()*math.pi/1000
                self.velENC2[self.arrayIDX] = self.enc_delta[1].get()*math.pi/100
                self.timeEntry += self.dataPeriod/1_000_000
                self.dataTime[self.arrayIDX] = self.timeEntry
                self.arrayIDX += 1
                if self.arrayIDX >= 300:
                    self.transition_to(S5_PRINT)
                elif self.ser.any():
                    char_in = self.ser.read(1).decode()                    
                    if(char_in == 's' or char_in == 'S'):
                        self.transition_to(S5_PRINT)
                    elif(char_in == 'M'):
                        print('Enter motor 2 duty cycle: ')
                        duty2 = self.user_input()
                        self.duty[1].write(duty2)

            elif self.state == S4_PRINT:
                for forTime1,forENC1,forVel1 in zip(self.dataTime[:self.arrayIDX],
                                                    self.dataENC1[:self.arrayIDX],
                                                    self.velENC1):
                    print('Time: {}; Pos.: {}; Vel.: {}'.format(round(forTime1,3),forENC1,forVel1))
                self.transition_to(S0_INIT)
                self.timeEntry = 0
                self.arrayIDX = 0

            elif self.state == S5_PRINT:
                for forTime2,forENC2,forVel2 in zip(self.dataTime[:self.arrayIDX],
                                                    self.dataENC2[:self.arrayIDX],
                                                    self.velENC2):
                    print('Time: {}; Pos.: {}; Vel.: {}'.format(round(forTime2,3),forENC2,forVel2))
                self.transition_to(S0_INIT)
                self.timeEntry = 0
                self.arrayIDX = 0
                
            else:
                raise ValueError('Invalid State')
            
            self.next_time = utime.ticks_add(self.next_time, self.dataPeriod)
    
    def transition_to(self, new_state):
        '''@brief      Transitions the FSM to a new state
           @details    Optionally a debugging message can be printed
                       if the dbg flag is set when the task object is created.
           @param      new_state The state to transition to.
        '''
        self.state = new_state
    
    def user_input(self):
        '''@brief      Deals with user input 
           @details    Reads one character at a time from user
        '''
        number = None
        ## String which is printed into the GUI window
        self.num_st = ''
        while True:
            if self.ser.any():
                char_in = self.ser.read(1).decode()
                if char_in.isdigit():
                    self.num_st += char_in
                    sys.stdout.write(char_in)
                elif char_in == '-':
                    if len(self.num_st) == 0:
                        self.num_st += char_in
                        sys.stdout.write(char_in)
                elif ord(char_in) == 127: 
                    if len(self.num_st) > 0: 
                        self.num_st = self.num_st[:-1]
                        sys.stdout.write('\b \b') 
                        sys.stdout.write('')
                elif char_in == '.':
                    if self.num_st.count('.') == 0:
                        self.num_st += char_in
                        sys.stdout.write(char_in)
                elif char_in == '\r':
                    print(' \n')
                    number = float(self.num_st)
                    return(number)
                    break            