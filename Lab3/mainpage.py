'''@file                mainpage.py
   @brief               Page describing work done on Lab 3
   
   
   @mainpage
   
   @section header3     Lab0x03 - DC Motor Control
                        The purpose of this lab is to control the DC motors in conjunction
                        with the encoder code written in Lab 2. This lab allows 
                        for control of the motor by altering the duty cycle, and
                        also allows for observation of the duty cycles effect
                        on motor position and velocity over time. 
                                              
   @section source3     Source code for Lab 3 is available here:
                        https://bitbucket.org/rykerbullis/me305/src/master/Lab3/
                        
   @section plots3      Motor Plots
                        The motor plots obtained with the code written in Lab 3
                        are pictured below. Motor position and velocity are plotted 
                        on the same graph for a period of 30 seconds. 
                        
                        \image html Lab3Motor1.png
                        \image html Lab3Motor2.png
'''