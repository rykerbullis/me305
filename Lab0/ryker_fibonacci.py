'''@file ryker_fibonacci.py
Created on Tue Sep 21 15:54:20 2021

@author: Ryker Bullis
'''

def fib(idx):
    # This section of code computes the Fibonacci number for given index "idx"
    # For loop computes Fibonacci sequence from bottom up until idx is reached
    n1, n2 = 0, 1               # Seed values for Fibonacci sequence
    for k in range(idx):
        n3 = n1 + n2            # Computes the next value in sequence
        n1 = n2                 # Update variable values for next loop
        n2 = n3
    return n1                   # Return the Fibonacci number for idx


    
if __name__ == '__main__':
# This section checks that the entered idx is a non-negative integer
# If idx is valid, the Fibonacci number is printed
    while True:
        # Prompt the user to enter an index
        indx = input('Enter the Fibonacci index: ')
        try:
            # Check that variable idx is an integer
            idx = int(indx)
        except:
            # Return warning if user input is not an integer
            print('Entered index must be an integer.')
        else:
            if int(indx)<0: # Check that idx is a positive number
                # Return warning if user input is negative
                print('Entered index must be non-negative.')                
            else: 
                # Compute the Fibonacci number if input is a non-negative integer
                fibnum = fib(idx)
                print('Fibonacci number at '
                      'index {:} is {:}.'.format(idx,fib(idx)))
                
                # Prompt the user to quit or continue after returning number
                out = input('Press any key to continue or press q to quit: ')
                if out == 'q':
                    # Quit program if correct key is pressed
                    break
                else:
                    # Prompt user for another index if quit is not pressed
                    continue