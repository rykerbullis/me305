''' @file imu.py
    @brief      Driver for BNO055 IMU
    @details    Driver which implements I2C to receive and transmit data to
                BNO055 IMU from Adafruit
    @author Ryker Bullis
    @author Jack Barone
    @date November 2, 2021
'''
import struct
import pyb
import os
import time



calib_len = bytearray(22)
calib_init = 0x55

euler_len = bytearray(6)
euler_init = 0x1A

gyr_len = bytearray(6)
gyr_init = 0x14

class IMU:
    ''' @brief 
        @details 
    '''
    def __init__(self,i2c):
        ''' @brief Constructs an IMU object
            @details
        '''
        self.i2c = i2c
        
    def mode(self,opr_mode):
        ''' @brief 
            @details
        '''
        # Set operating mode
        self.i2c.mem_write(opr_mode,0x28,0x3D)

    def get_status(self):
        ''' @brief 
            @details
        '''
        # CALIB_STAT
        calib_buff = bytearray(1)
        self.i2c.mem_read(calib_buff,0x28,0x35)
        status = ( calib_buff[0] & 0b11,
                  (calib_buff[0] & 0b11 << 2) >> 2,
                  (calib_buff[0] & 0b11 << 4) >> 4,
                  (calib_buff[0] & 0b11 << 6) >> 6)
        return status
        
    def get_constants(self,):
        ''' @brief 
            @details
        '''
        self.i2c.mem_read(calib_len,0x28,calib_init)
        return(calib_len)
    
    def write_constants(self,constants):
        ''' @brief 
            @details
        '''
        self.imu_cal_bytearray = bytearray(constants)
        self.i2c.mem_write(self.imu_cal_bytearray,0x28,calib_init)
        
    def read_euler(self):
        ''' @brief 
            @details
        '''
        eul_angles = self.i2c.mem_read(euler_len,0x28,euler_init)
        eul_signed = struct.unpack('<hhh', eul_angles)
        eul_val = tuple(eul_int/16 for eul_int in eul_signed)
        return eul_val
        
    def read_angvel(self):
        ''' @brief 
            @details
        '''
        omega = self.i2c.mem_read(gyr_len,0x28,gyr_init)
        omega_signed = struct.unpack('<hhh', omega)
        omega_val = tuple(-omega_int/16 for omega_int in omega_signed)
        return omega_val

