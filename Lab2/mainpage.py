'''@file                mainpage.py
   @brief               Page describing work done on Lab 2
                        
   @mainpage

   @section header2     Lab0x02 - Incremental Encoders
                        The purpose of this lab is to read the encoder position
                        and record data from the encoders over a period of time. 
                        The lab uses encoder driver and task files in order to 
                        track changes in motor position and speed. 
                        
   @section source2     Source code for Lab 2 is available here:
                        https://bitbucket.org/rykerbullis/me305/src/master/Lab2/
                        
   @section diagrams2   State Transition and Task Diagrams
                        A link to a OneDrive folder with pdf documents of the applicable
                        task and state transition diagrams for Lab 2 are available
                        here:
                        https://cpslo-my.sharepoint.com/:f:/g/personal/jbarone_calpoly_edu/EgJWo3pkMp5Jk4R3YWhTAlYBa8rI6C3ZJtjnHclHs-RhxQ
                        
                        \image html Lab2TaskDiagram.PNG "Task Diagram"
                        \image html Lab2TaskEnc.PNG "Task Encoder State Transition Diagram"
                        \image html Lab2TaskUser.PNG "Task User State Transition Diagram"
'''