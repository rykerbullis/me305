S0_INIT                  = 0
S1_WAIT_FOR_INPUT        = 1
S2_DISPLAY_SQUARE        = 2
S3_DISPLAY_SINE          = 3
S4_DISPLAY_SAWTOOTH      = 4

class Task_Example: 
    def __init__(self): 
        #constructor - runs when an object is instantiated from this class
        self.state = S0_INIT
        
        self.runs = 0
    
    def run(self):
        if (self.state== S0_INIT):
            print('running state 0')
            self.transisiton_to(S1_WAIT_FOR_INPUT)
            
        elif (self.state==S1_WAIT_FOR_INPUT):
            print ('running state 1')
            self.transisiton_to(S2_DISPLAY_SQUARE)
            
        elif (self.state==S2_DISPLAY_SQUARE):
            print ('running state 2')
            self.transisiton_to(S3_DISPLAY_SINE)
            
        elif (self.state==S3_DISPLAY_SINE):
            print ('running state 3')
            self.transisiton_to(S4_DISPLAY_SAWTOOTH)
            
        elif (self.state==S4_DISPLAY_SAWTOOTH):
            print ('running state 4')
            self.transisiton_to(S2_DISPLAY_SQUARE)
            
        self.runs += 1
        
    def transition_to(self, new_state): 
        print(self.state, '->', new_state)
        self.state = new_state 
        