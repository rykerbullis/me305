import pyb

pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
pinC6 = pyb.Pin(pyb.Pin.cpu.C6)
pinC7 = pyb.Pin(pyb.Pin.cpu.C7)

tim4 = pyb.Timer(4, prescaler=0, period=65535)
t4ch1 = tim4.channel(1,pyb.Timer.ENC_A,pin=pinB6)
t4ch2 = tim4.channel(2,pyb.Timer.ENC_B,pin=pinB7)

tim3 = pyb.Timer(3, prescaler=0, period=65535)
t3ch1 = tim3.channel(1,pyb.Timer.ENC_A,pin=pinC6)
t3ch2 = tim3.channel(2,pyb.Timer.ENC_B,pin=pinC7)
   

if __name__ == '__main__':

    while(True):
        try:
            print("ENC A {}, ENC B {}".format(tim3.counter(),tim4.counter()))
                
        except KeyboardInterrupt:
            break
    print('Program Terminating')

    

