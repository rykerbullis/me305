''' @file task_TP.py
    @brief Responsible for getting ball position from touchpanel
    @details Uses alpha-beta filter to calculate velocity and smooth 
             position readings
             Source code available here: 
             https://bitbucket.org/jbarone1/me305/src/master/Term%20Project/
    @author Ryker Bullis
    @author Jack Barone
    @date December 7, 2021
'''
import os
import utime 

S0_WAIT            = 0
S1_FILTER          = 1
S2_CHECK           = 2
 
class Task_TP: 
    '''@brief Responsible for getting ball position from touchpanel
       @details Uses alpha-beta filter to calculate velocity and smooth 
                position readings
    '''
    def __init__(self,touchpanel,position_vector,run_flag):
        '''@brief Constructs touchpannel task 
           @details Task used to construct touchpanel
           @param touchpanel      Touchpanel object imported from main.py
           @param position_vector Shares object that velocity and position are
                                  written to
           @param run_flag        Flag used to indicate controller is running,
                                  not used
        '''
        ## Touchpanel starts and waits for an input in this state
        self.state = S0_WAIT
        ## Touchpanel object 
        self.touchpanel = touchpanel
        ## Position vector, [X, Xdot, Y, Ydot]
        self.position_vector = position_vector
        ## Boolean flag indicating controller is running
        self.run_flag = run_flag
        ## Alpha gain used in filter
        self.alpha = 0.6    
        ## Beta gain used in filter 
        self.beta = 0.008
        ## Counter used for touchpanel logic
        self.n = 0
        ## Start time used to calculate time between tasks
        self.startTime = utime.ticks_us()
        ## Run touchpanel calibration 
        self.run_calibration()
        
        
        
    def run(self): 
        '''@brief   Runs alpha-beta filter to find ball velocity and position
           @details Uses previous position to calculate an estimate of next 
                    position and velocity. 
        '''
        position = self.touchpanel.scanXY() 
        self.X = position[0] / 1000
        self.Y = position[1] / 1000
        self.Z = self.touchpanel.scanZ()
        
        self.stopTime = utime.ticks_us() 
        self.T = utime.ticks_diff(self.stopTime,self.startTime) /1000000
        if self.state == S0_WAIT:
            self.X_estimate = 0
            self.Vx_estimate = 0
            self.Y_estimate = 0
            self.Vy_estimate = 0
            if self.Z == True:
                self.transition_to(S1_FILTER)
                
        elif self.state == S1_FILTER:
            if self.touchpanel.scanZ() == True:
                self.X_estimate = (self.X_prev + self.alpha*(self.X - self.X_prev) + self.T * self.Vx_prev) 
                self.Vx_estimate = (self.Vx_prev + self.beta*(self.X - self.X_prev)/self.T ) 
                
                self.Y_estimate = (self.Y_prev + self.alpha*(self.Y - self.Y_prev) + self.T * self.Vy_prev) 
                self.Vy_estimate = (self.Vy_prev + self.beta*(self.Y - self.Y_prev)/self.T ) 
            if self.touchpanel.scanZ() == False : 
                self.X_estimate = (self.X_prev + self.T * self.Vx_prev) 
                self.Vx_estimate = (self.Vx_prev ) 
                self.n += 1
                if self.n == 5:
                    self.n=0
                    self.transition_to(S0_WAIT)
        
    
        self.X_prev = self.X_estimate
        self.Vx_prev = self.Vx_estimate
        self.Y_prev = self.Y_estimate
        self.Vy_prev = self.Vy_estimate
        
        self.position_vector[0].write(self.X_estimate)
        self.position_vector[1].write(self.Vx_estimate)
        self.position_vector[2].write(self.Y_estimate)
        self.position_vector[3].write(self.Vy_estimate)
        self.startTime = utime.ticks_us()
        
        
        #
        

        
        # if self.state == S0_WAIT:
        #     self.X_estimate = 0
        #     self.Vx_estimate = 0
        #     self.Y_estimate = 0
        #     self.Vy_estimate = 0
            
        #     if self.touchpanel.scanZ() == True:
        #         self.Vx_prev = 0
        #         self.X_prev = X
        #         self.Vy_prev = 0
        #         self.Y_prev = Y
        #         self.transition_to(S1_FILTER)            
                
        # elif self.state == S1_FILTER:
        #     if self.touchpanel.scanZ() == False:
        #         self.transition_to(S2_CHECK)
            
        #     self.X_estimate = (self.X_prev + self.alpha*(X - self.X_prev) + self.T * self.Vx_prev) 
        #     self.Vx_estimate = (self.Vx_prev + self.beta*(X - self.Vx_prev)/self.T ) 
            
        #     self.Y_estimate = (self.Y_prev + self.alpha*(Y - self.Y_prev) + self.T * self.Vy_prev) 
        #     self.Vy_estimate = (self.Vy_prev + self.beta*(Y - self.Vy_prev)/self.T ) 

        #         #self.position_vector.put(X_estimate, V_estimate , 0,0)
        #     self.X_prev = self.X_estimate
        #     self.Vx_prev = self.Vx_estimate
        #     self.Y_prev = self.Y_estimate
        #     self.Vy_prev = self.Vy_estimate
            
        # elif self.state == S2_CHECK:
        #     if self.touchpanel.scanZ() == False:
        #         self.X_estimate = self.X_prev
        #         self.Vx_estimate = self.Vx_prev
        #         self.Y_estimate = self.Y_prev
        #         self.Vy_estimate = self.Vy_prev
        #         self.n += 1
        #         if self.n > 4:
        #             self.transition_to(S0_WAIT)
        #     if self.touchpanel.scanZ() == True:
        #         self.X_prev = self.X_estimate
        #         self.Vx_prev = self.Vx_estimate
        #         self.Y_prev = self.Y_estimate
        #         self.Vy_prev = self.Vy_estimate
        #         self.n = 0
        #         self.transition_to(S1_FILTER)

        # self.position_vector[0].write(self.X_estimate)
        # self.position_vector[1].write(self.Vx_estimate)
        # self.position_vector[2].write(self.Y_estimate)
        # self.position_vector[3].write(self.Vy_estimate)
        # self.startTime = utime.ticks_us()
            
            
    def run_calibration(self):
        '''@brief   Runs calibration procedure to calibrate touch pannel
           @details First checks to see if .txt file with calibrations data is 
                    present. If it is, it will write those to the TP driver. 
                    If .txt file is not present, it will run calibration.
        '''
        filename = "RT_cal_coeffs.txt"
        if filename in os.listdir():
            with open(filename,'r') as f:
                cal_data_string = f.readline()
                cal_values = [float(cal_value) for cal_value in cal_data_string.strip().split(',')]
                self.touchpanel.writeConstants(cal_values)
        else:
            with open(filename, 'w') as f:  
                (Kxx, Kxy, Kyx, Kyy, Xc, Yc) = self.touchpanel.calibration()
                f.write(f"{Kxx}, {Kxy}, {Kyx}, {Kyy}, {Xc}, {Yc}\r\n")
        
    def transition_to(self, new_state):
        '''@brief      Transitions the FSM to a new state
           @details    Optionally a debugging message can be printed
                       if the dbg flag is set when the task object is created.
           @param      new_state The state to transition to.
        '''
        self.state = new_state