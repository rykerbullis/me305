'''@file task_user.py
   @brief User interface for encoder and motor operation
   @details    Implements a finite state machine for the user task. Prompts
               the user for inputs and returns data where applicable. 
               Source code available here:
               https://bitbucket.org/rykerbullis/me305/src/master/Lab4/
   @author Ryker Bullis
   @author Jack Barone
   @date December 4, 2021
'''
import utime
import pyb
import sys
import array

S0_INIT             = 0
S1_IDLE             = 1
S2_DIRECTIONS       = 2
S3_WAIT             = 3
S4_COLLECT          = 4
S5_PRINT            = 5

class Task_User:
    ''' @brief User UI for TERM Project
        @details    Implements a finite state machine for the user task. Cycles 
                    through collection, printing, and additional states. 
    '''
    
    def __init__(self,run_flag,panel_vector,position_vector):
        ''' @brief      Initializes the user task
            @details    Receives and initializes constructor to run task_user
            @param run_flag         Shares flag which enables and disables motors
            @param panel_vector     Tuple containing platform orientation data
            @param position_vector  Tuple containing ball orientation data
        '''
        self.state = S0_INIT
        self.ser = pyb.USB_VCP()  
        self.dataPeriod = 0.1
        self.num_st = ''
        ## Flag which enables and disables motors
        self.run_flag = run_flag
        ## Tuple containing the position and velocity of the ball in the X and Y directions
        self.panel_vector = panel_vector
        ## Tuple containing the position and velocity of the ball in the X and Y directions
        self.position_vector = position_vector
        
        ## Create an empty array for x position to record in
        self.dataX = array.array('f', 1000*[0])
        ## Create an empty array for x_dot position to record in
        self.dataX_DOT = array.array('f', 1000*[0])
        ## Create an empty array for theta_y position to record in
        self.dataTHETA_Y = array.array('f', 1000*[0])
        ## Create an empty array for theta_dot_y to record in
        self.dataTHETA_DOT_Y = array.array('f', 1000*[0])
        ## Create an empty array for the time to record in
        self.dataTime = array.array('f', 1000*[0])
        ## Array index for assigning data
        self.arrayIDX = 0
        # Create time step tracker variable
        self.timeEntry = 0
        
        
    def run(self):
        ''' @brief      Runs the user task
            @details    Receives user input and returns data, disables the platform
                        if requested by the user.
        '''
        if self.state == S0_INIT:
            print(' Welcome to the ball balancing program! \n',
                  'Press E to enable the platform.')
            self.transition_to(S1_IDLE)
            
        elif self.state == S1_IDLE:
            if self.ser.any():
                char_in = self.ser.read(1).decode()
                if(char_in == 'e' or char_in == 'E'):
                    print('Motors enabled, platform is active.')
                    self.run_flag.write(True)
                    self.transition_to(S2_DIRECTIONS)
                    
        
                
        elif self.state == S2_DIRECTIONS:
            print(' Press b to collect 10 seconds of data. \n',
                  'Press s to stop data collection early. \n',
                  'Press c to disable the platform.')
            self.transition_to(S3_WAIT)
    
        elif self.state == S3_WAIT:
            if self.ser.any():
                ## Variable representing keyboard inputs to advance FSM
                char_in = self.ser.read(1).decode()
                # Print 10s of ball data
                if(char_in == 'b' or char_in == 'B'):
                    print('Collecting 10s of data...')
                    self.transition_to(S4_COLLECT)
                if(char_in == 'c' or char_in == 'C'):
                    self.run_flag.write(False)
                    print('Platform disabled.')
                    self.transition_to(S0_INIT)
        
        elif self.state == S4_COLLECT:
            self.dataX[self.arrayIDX] = self.position_vector[0].read()
            self.dataX_DOT[self.arrayIDX] = self.position_vector[1].read()
            self.dataTHETA_Y[self.arrayIDX] = self.panel_vector[2].read()
            self.dataTHETA_DOT_Y[self.arrayIDX] = self.panel_vector[3].read()
            
            self.dataTime[self.arrayIDX] = self.timeEntry
            
            self.arrayIDX += 1
            self.timeEntry += self.dataPeriod
            
            if self.arrayIDX >= 1000:
                self.transition_to(S5_PRINT)
            elif self.ser.any():
                char_in = self.ser.read(1).decode()                    
                if(char_in == 's' or char_in == 'S'):
                    self.transition_to(S5_PRINT)
                    
        elif self.state == S5_PRINT:
            print('Time,    X,    X_DOT,    THETA_Y,    THETA_DOT_Y')
            for forTime,forX,forXd,forTy,forTyd in zip(self.dataTime[:self.arrayIDX],
                                                       self.dataX[:self.arrayIDX],
                                                       self.dataX_DOT[:self.arrayIDX],
                                                       self.dataTHETA_Y[:self.arrayIDX],
                                                       self.dataTHETA_DOT_Y[:self.arrayIDX]):
                print('{},{},{},{},{}'.format(forTime,forX,forXd,forTy,forTyd))
            print('Data collected and saved.')
            self.transition_to(S0_INIT)
            self.timeEntry = 0
            self.arrayIDX = 0
            
    def transition_to(self, new_state):
        '''@brief      Transitions the FSM to a new state
           @details    Optionally a debugging message can be printed
                       if the dbg flag is set when the task object is created.
           @param      new_state The state to transition to.
        '''
        self.state = new_state
    
    def user_input(self):
        '''@brief      Deals with user input 
           @details    Reads one character at a time from user
        '''
        # String which is printed into the GUI window
        number = None
        if self.ser.any():
            char_in = self.ser.read(1).decode()
            if char_in.isdigit():
                self.num_st += char_in
                sys.stdout.write(char_in)
                return None
            elif char_in == '-':
                if len(self.num_st) == 0:
                    self.num_st += char_in
                    sys.stdout.write(char_in)
                    return None
            elif ord(char_in) == 127: 
                if len(self.num_st) > 0: 
                    self.num_st = self.num_st[:-1]
                    sys.stdout.write('\b \b') 
                    sys.stdout.write('')
                    return None
            elif char_in == '.':
                if self.num_st.count('.') == 0:
                    self.num_st += char_in
                    sys.stdout.write(char_in)
                    return None
            elif char_in == '\r':
                print(' \n')
                number = float(self.num_st)
                self.num_st = ''
                return(number)