''' @file task_imu.py
    @brief      Task for calibration of the IMU on system startup
    @details    Calibrates the IMU by importing a file, or writing to a file 
                if the calibration data is not available on the Nucleo board. 
                Writes the euler angles and velocities 
    @author Ryker Bullis
    @author Jack Barone
    @date December 1, 2021
'''

import imu
import pyb
import os

## Configuration mode for the BNO055
mode_config = 0x00
## NDOF mode for the BNO055
mode_NDOF = 0x0C
## NDOF OFF mode for the BNO055
mode_NDOF_OFF = 0x0B

# States
S0_INIT     = 0
S1_EULER    = 1

class Task_IMU:
    ''' @brief      Calibrates IMU and returns orientation data
        @details    Checks that the panel is calibrated and writes calibration 
                    coefficients if necessary. 
    '''

    def __init__(self,panel_vector):
        ''' @brief Initializes IMU task
            @details Initializes the I2C class and constructors for the calibration
                     of the IMU. 
            @param panel_vector Shares object which contains the angular position
                                and velocity of the platform in the XY plane
        '''
        ## Instatiates the master for the I2C protocol
        self.i2c = pyb.I2C(1,pyb.I2C.MASTER)
        ## Creates a driver object
        self.IMU_driver = imu.IMU(self.i2c)
        self.i2c.mem_write(0x05,0x28,0x3B)      # Unit selection
        self.IMU_driver.mode(mode_NDOF_OFF)     # Set mode to NDOF
        ## Sets the initial state of the FSM
        self.state = S0_INIT
        ## Tuple containing the platform angular positions and velocities. 
        self.panel_vector = panel_vector
        
        self.run_calibration()
        
    def run_calibration(self):
        ''' @brief      Runs the calibration sequence
            @details    Calibrates the IMU by importing calibration coefficients 
                        or recalibrating the IMU and writing the data to a file.
        '''
        ## Current status of the IMU
        status = self.IMU_driver.get_status()
        
        print('WELCOME TO THE BALL BALANCING PROGRAM! \n',
              'Checking calibration...')
        
        ## File for the IMU calibration coefficient            
        filename = "IMU_cal_coeffs.txt"
        if filename in os.listdir():
            print('Fetching calibration data...')
            self.IMU_driver.mode(mode_config)
            with open(filename,'r') as f:
                imu_data_string = f.readline()
                imu_cal_values = [int(cal_value) for cal_value in imu_data_string.strip().split(',')]
                self.IMU_driver.write_constants(imu_cal_values)
            self.IMU_driver.mode(mode_NDOF)
            
        else:
            print('CALIBRATION REQUIRED. Rotate platform until fully calibrated.')
            while status[0] + status[1] + status[2] + status[3] != 12:
                status = self.IMU_driver.get_status()
                print(status)
            print("Fully Calibrated")
            with open(filename, 'w') as f:
                (K1,K2,K3,K4,K5,K6,K7,K8,K9,K10,K11,K12,K13,K14,K15,K16,K17,K18,K19,K20,K21,K22) = self.IMU_driver.get_constants()
                f.write(f"{K1},{K2},{K3},{K4},{K5},{K6},{K7},{K8},{K9},{K10},{K11},{K12},{K13},{K14},{K15},{K16},{K17},{K18},{K19},{K20},{K21},{K22}\r\n")
                
        
    def run(self):
        ''' @brief      Return Euler angles and velocities from the IMU
            @details    Reads the euler angle and velocity of the platform using
                        the IMU driver, then writes them to the panel data tuple.
        '''
        ## Euler angle of the platform
        euler = self.IMU_driver.read_euler()
        ## Angular velocity of the platform
        vel = self.IMU_driver.read_angvel()
            
        # ( theta_x , theta_dot_x , theta_y , theta_dot_y )
        self.panel_vector[0].write(-euler[1])
        self.panel_vector[1].write(-vel[1])
        self.panel_vector[2].write(-euler[2])
        self.panel_vector[3].write(-vel[2])

    def transition_to(self, new_state):
        '''@brief      Transitions the FSM to a new state
           @details    Optionally a debugging message can be printed
                       if the dbg flag is set when the task object is created.
           @param      new_state The state to transition to.
        '''
        self.state = new_state
