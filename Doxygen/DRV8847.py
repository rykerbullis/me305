''' @file DRV8847.py
    @brief Motor driver to enable, disable, set duty cycle
    @details    Implements functionality to recognize faults, set motor duty
                cycle, and control a fault flag
                Source code available here:
                https://bitbucket.org/rykerbullis/me305/src/master/Lab4/
    @author Jack Barone
    @author Ryker Bullis
    @date November 1, 2021
'''

import pyb
import utime

class DRV8847:
    ''' @brief A motor driver class for the DRV8847 from TI.
        @details    Objects of this class can be used to configure the DRV8847
                    motor driver and to create one or more objects of the
                    Motor class which can be used to perform motor
                    control.
                    Refer to the DRV8847 datasheet here:
                    https://www.ti.com/lit/ds/symlink/drv8847.pdf
    '''
    
    def __init__ (self, timer, frequency):
        ''' @brief      Initializes and returns a DRV8847 object.
            @param sleep_pin    Nucleo pin associated with sleep state
            @param sleep_out    Constructor used to enable and disable motor
            @param fault_pin    Nucleo pin associated with fault, returns falling edge
            @param fault_flag   Constructor used to indicate a fault
            @param timer        Timer used for motor (TIM3 on hardware)
            @param frequency    Frequency of motor (20kHz)
        '''
        ## Timer used to control the motor
        self.TIMER = pyb.Timer(timer, freq=frequency)
        ## Constructor which sets the pin used to enable and disable the motor
        #self.nSLEEP = pyb.Pin(sleep_pin,sleep_out)
        ## Boolean flag which becomes "true" when a fault is detected
        ##self.fault_flag = fault_flag
        ## Pin associated with fault at the hardware level
        #self.fault_pin = fault_pin
        
        ##faultInt = pyb.ExtInt(self.fault_pin,mode=pyb.ExtInt.IRQ_FALLING,
        #              pull=pyb.Pin.PULL_NONE,callback=self.fault_cb)

    # def enable (self):
    #     ''' @brief      Brings the DRV8847 out of sleep mode.
    #     '''
    #     self.fault_flag.write(True)             # Disable fault interrupt
    #     self.nSLEEP.high()                      # Re-enable the motor driver
    #     utime.sleep_us(25)                      # Wait for the fault pin to return high
    #     self.fault_flag.write(False)            # Re-enable the fault interrupt
        
    # def disable (self):
    #     ''' @brief      Puts the DRV8847 in sleep mode.
    #     '''
    #     self.nSLEEP.low()
    
    # def fault_cb (self, IRQ_src):
    #     ''' @brief      Callback function to run on fault condition.
    #         @param      IRQ_src The source of the interrupt request.
    #     '''
    #     self.fault_flag.write(True)
    
    def motor (self,in_A, chA, in_B, chB):
        ''' @brief      Initializes and returns a motor object associated with 
                        the DRV8847.
            @param in_A Pin 1 for motor
            @param chA  Channel 1 for motor
            @param in_B Pin 2 for motor
            @param chB  Channel 2 for motor            
            @return     An object of class Motor
        '''
        self.IN_A = in_A
        self.IN_B = in_B
        t_chA = self.TIMER.channel(chA,pyb.Timer.PWM,pin=self.IN_A)
        t_chB = self.TIMER.channel(chB,pyb.Timer.PWM,pin=self.IN_B)
        motor = Motor(t_chA, t_chB)
        return motor

class Motor:
     ''' @brief         A motor class for one channel of the DRV8847.
         @details       Objects of this class can be used to apply PWM to a given
                        DC motor.
     '''

     def __init__ (self, t_chA, t_chB):
         ''' @brief     Initializes and returns a motor object associated with the DRV8847.
             @details   Objects of this class should not be instantiated
                        directly. Instead create a DRV8847 object and use
                        that to create Motor objects using the method
                        DRV8847.motor().
             @param t_chA   Channel 1
             @param t_chB   Channel 2
         '''
         self.t_chA = t_chA
         self.t_chB = t_chB

     def set_duty (self, duty):
         ''' @brief     Set the PWM duty cycle for the motor channel.
             @details   This method sets the duty cycle to be sent
                        to the motor to the given level. Positive values
                        cause effort in one direction, negative values
                        in the opposite direction.
            @param      duty    A signed number holding the duty
                                cycle of the PWM signal sent to the motor
         '''
         if (duty >= 0):
             self.t_chA.pulse_width_percent(duty)
             self.t_chB.pulse_width_percent(0)
         elif (duty <= 0):
             self.t_chA.pulse_width_percent(0)
             self.t_chB.pulse_width_percent(abs(duty))
