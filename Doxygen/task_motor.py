'''@file task_motor.py
   @brief Responsible for motor control
   @details    Controls motor by setting duty cycle
               Source code available here:
               https://bitbucket.org/rykerbullis/me305/src/master/Lab4/
   @author Ryker Bullis
   @author Jack Barone
   @date October 29, 2021
'''

class Task_Motor:   
    '''@brief Sets the duty cycle of the motors in order to control speed. 
    
    '''
    def __init__(self,run_flag,motor_drv,duty,motor): 
        '''@brief Constructs motor task
           @details Task used to control the motor
           @param run_flag      Boolean object which becomes true during enable state
           @param motor_drv     Constructor which calls motor driver
           @param duty          Duty cycle of the motor
           @param motor         Method in motor driver
        '''
        
        ## Flag which enables and disables motors
        self.run_flag = run_flag
        ## Constructor calling method of DRV8847
        self.motor = motor
        ## Duty cycle of the motor
        self.duty = duty
        ## Calls motor driver, set up in main
        self.motor_drv = motor_drv

        
    def run(self):
        '''@brief
        '''
        if self.run_flag.read() == True:    
            self.motor.set_duty(int(self.duty.read()))
        else:
            self.motor.set_duty(0)
