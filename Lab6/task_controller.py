''' @file task_controller.py
    @brief          Controller for the ball balancing platform
    @details        Uses data from the IMU and touch panel to control the platform
                    angle and velocity. Implements a state space model of the 
                    platform dynamics for control. 
    @author Ryker Bullis
    @author Jack Barone
    @date December 8, 2021
'''

import utime
from ulab import numpy as np

## Motor torque constant
K_T = 13.8*10**(-3)                 # [N-m/A]
## Motor resistance
R = 2.21                            # [Ohm]
## Motor DC voltage
V_DC = 12                           # [V]
## Duty cycle gain
K_duty = (100*R)/(4*K_T*V_DC)       # [%/N-m]

class Task_Controller:
    ''' @brief          Controls the plaform angle and velocity
        @details        Uses IMU data to control the plaform and manipulate the
                        position of the ball
    '''
 
    def __init__(self,position_vector,panel_vector,duty):
        ''' @brief      Initializes the controller task
            @details    Imports the constructors and initializes the controller 
                        task
        '''
        ## Tuple containing the position and velocity of the ball
        self.position_vector = position_vector
        ## Tuple containing the angle and angular velocity of the platform
        self.panel_vector = panel_vector
        ## Tuple containing the duty cycle of both motors
        self.duty = duty
        
    def run(self):
        ''' @brief      Runs the controller
            @details    Reads the current position and velocity of the ball and 
                        platform, then outputs a duty cycle to both motors to 
                        move the ball back to the zero position.
        '''
        
        ## Position of the ball in the X direction
        self.x = self.position_vector[0].read()
        ## Velocity of the ball in the X direction 
        self.x_dot = self.position_vector[1].read()
        ## Position of the ball in the Y direction
        self.y = self.position_vector[2].read()
        ## Velocity of the ball in the Y direction
        self.y_dot = self.position_vector[3].read()
            
        ## Angle of the platform in the X direction
        self.theta_x = self.panel_vector[0].read()
        ## Angular velocity of the platform in the X direction
        self.theta_dot_x = self.panel_vector[1].read()
        ## Angle of the platform in the Y direction
        self.theta_y = self.panel_vector[2].read()
        ## Angular velocity of the platform in the Y direction
        self.theta_dot_y = self.panel_vector[3].read()
        
        # X DIRECTION TUNING
        ## Gain corresponding to x
        K1y =  4.5 / 1.1
        ## Gain corresponding to theta_y
        K2y = 1.5 / 1
        ## Gain corresponding to x_dot
        K3y =  1.15 / 1
        ## Gain corresponding to theta_dot_y
        K4y = 0.085 / 1
        
        # Y DIRECTION TUNING
        ## Gain corresponding to x
        K1x = -4.9 / 1.1
        ## Gain corresponding to theta_x
        K2x = 2.1
        ## Gain corresponding to y_dot
        K3x = -2.1 / 1
        ## Gain corresponding to theta_dot_x
        K4x = .13
        
        ## Motor torque for the Y axis
        T_y = K1y*self.x + K2y*self.theta_y + K3y*self.x_dot + K4y*self.theta_dot_y
        ## Motor torque for the X axis
        T_x = K1x*self.y + K2x*self.theta_x + K3x*self.y_dot + K4x*self.theta_dot_x
        
        ## Duty cycle for the X axis
        D_x = K_duty*T_x
        ## Duty cycle for the Y axis
        D_y = K_duty*T_y
        
        if D_x >= 80 :
            D_x = 80
        if D_x <= -80:
            D_x = -80
        if D_y >= 80:
            D_y = 80
        if D_y <= -80:
            D_y = -80
            
        self.duty[0].write(D_x)
        self.duty[1].write(D_y)