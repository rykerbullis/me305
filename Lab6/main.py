''' @file main.py
    @brief       Main file ball balancing platform
    @details     Runs tasks associated with closed loop control of ball 
                 balncing platform
                 Source Code: 
                 https://bitbucket.org/jbarone1/me305/src/master/Term%20Project/
    @author Jack Barone
    @author Ryker Bullis
    @date December 7, 2021                
'''
import pyb 
import shares
import DRV8847
import touchpanel
import task_motor 
import task_TP
import task_imu
import task_controller
import task_user

## Instantiates run flag shares object
run_flag = shares.Share(False)
## Instantiates a touple of shares objects used for duty cycle of motor 1 + 2
duty = (shares.Share(0),shares.Share(0))

#touchpanel output is a position vector, [x_b,Vx_b,y_b,Vy_b]
position_vector = (shares.Share(),shares.Share(),shares.Share(),shares.Share())

## Instantiates motor driver by calling motor driver function
motor_drv = DRV8847.DRV8847(3,20000)
## Instantiates pin used for control of motor 1
IN1 = pyb.Pin(pyb.Pin.cpu.B4)
## Instantiates second pin used for control of motor 1
IN2 = pyb.Pin(pyb.Pin.cpu.B5)
## Instantiates pin used for control of motor 2
IN3 = pyb.Pin(pyb.Pin.cpu.B0)
## Instantiates second pin used for control of motor 2
IN4 = pyb.Pin(pyb.Pin.cpu.B1)

## Instantiate motor 1
motor_1 = motor_drv.motor(IN1, 1, IN2, 2)
## Instantiate motor 2
motor_2 = motor_drv.motor(IN3, 3, IN4, 4)

## Instantiates pin A0 used for TP scans
pinA0 = pyb.Pin(pyb.Pin.cpu.A0)
## Instantiates pin A1 used for TP scans
pinA1 = pyb.Pin(pyb.Pin.cpu.A1)
## Instantiates pin A6 used for TP scans
pinA6 = pyb.Pin(pyb.Pin.cpu.A6)
## Instantiates pin A7 used for TP scans
pinA7 = pyb.Pin(pyb.Pin.cpu.A7)

## Instantiates xp used for TP scans
xp = pyb.Pin(pinA7,mode=pyb.Pin.OUT_PP)
## Instantiates xm used for TP scans
xm = pyb.Pin(pinA1,mode=pyb.Pin.OUT_PP)
## Instantiates yp used for TP scans
yp = pyb.Pin(pinA0,mode=pyb.Pin.IN)
## Instantiates ym used for TP scans
ym = pyb.Pin(pinA6,mode=pyb.Pin.IN)

## Instantiates touchpanel object using touchpanel driver
touchpanel = touchpanel.TouchPanel(pinA0,pinA7, xp,xm,yp,ym)

## Panel_vector is a touple of shares objects containing imu outputs 
panel_vector = (shares.Share(),shares.Share(),shares.Share(),shares.Share())

run_flag = (shares.Share(False))

if __name__ == '__main__': 
    ## Task responsible for touchpanel
    task1 = task_TP.Task_TP(touchpanel,position_vector,run_flag)
    ## Task responsible for IMU
    task2 = task_imu.Task_IMU(panel_vector)
    ## Task responsible for State Space control
    task3 = task_user.Task_User(run_flag,panel_vector,position_vector)
    task4 = task_controller.Task_Controller(position_vector, panel_vector, duty)
    ## Task responsible for motor 1
    task5 = task_motor.Task_Motor(run_flag,motor_drv,duty[1], motor_1)
    ## Task responsible for motor 2
    task6 = task_motor.Task_Motor(run_flag,motor_drv,duty[0], motor_2)
    
     
    while(True): 
        try: 

            task1.run()
            task2.run()
            task3.run()
            task4.run()
            task5.run()
            task6.run()
            #print(duty[0].read(),duty[1].read())
            #print(position_vector[0].read(),position_vector[2].read())
            #print(position_vector[0].read(),position_vector[1].read(),position_vector[2].read(),position_vector[3].read())
            #print(panel_vector[0].read(),panel_vector[1].read(),panel_vector[2].read(),panel_vector[3].read())
        except KeyboardInterrupt: 
            #duty[1].write(0)
            #duty[0].write(0)
            #task4.run()
            #task5.run()
            break 
    print('Program Terminating')
