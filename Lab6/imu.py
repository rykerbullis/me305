''' @file imu.py
    @brief      Driver for BNO055 IMU
    @details    Driver which implements I2C to receive and transmit data to
                BNO055 IMU from Bosch
    @author Ryker Bullis
    @author Jack Barone
    @date December 12, 2021
'''
import struct

calib_len = bytearray(22)
calib_init = 0x55

euler_len = bytearray(6)
euler_init = 0x1A

gyr_len = bytearray(6)
gyr_init = 0x14

class IMU:
    ''' @brief  IMU class for BNO055 IMU from Bosch
        @details Objects of this class can be used to initilize IMU, set mode,
        get status, get calibration constants, write constants, and read the 
        euler angles and velocities. See data sheet here:
        https://cdn-shop.adafruit.com/datasheets/BST_BNO055_DS000_12.pdf

    '''
    def __init__(self,i2c):
        ''' @brief Constructs an i2c IMU object  
            @details 
        '''
        self.i2c = i2c
        
    def mode(self,opr_mode):
        ''' @brief Select operating mode of IMU
            @details All operating modes can be found on page 21 
            @param opr_mode hexadecimal value indicating operating mode
        '''
        self.i2c.mem_write(opr_mode,0x28,0x3D)

    def get_status(self):
        ''' @brief Reads and returns calibration of status of IMU
            @details Returns status of IMU. IMU must be fully calibrated before 
            proper operation
        '''
        calib_buff = bytearray(1)
        self.i2c.mem_read(calib_buff,0x28,0x35)
        status = ( calib_buff[0] & 0b11,
                  (calib_buff[0] & 0b11 << 2) >> 2,
                  (calib_buff[0] & 0b11 << 4) >> 4,
                  (calib_buff[0] & 0b11 << 6) >> 6)
        return status
        
    def get_constants(self,):
        ''' @brief Reads and returns calibration constants of IMU
            @details Returns a bytearray of calibration constants which can be
            stored and written to a text file.
        '''
        self.i2c.mem_read(calib_len,0x28,calib_init)
        return(calib_len)
    
    def write_constants(self,constants):
        ''' @brief Writes calibration constants to IMU
            @details Takes in a bytearray of calibrations constants and writes
            to the IMU. This function allows IMU calibration without the need 
            physically calibrate the IMU. 
            @param constants Bytearray of IMU calibration constants 
        '''
        self.imu_cal_bytearray = bytearray(constants)
        self.i2c.mem_write(self.imu_cal_bytearray,0x28,calib_init)
        
    def read_euler(self):
        ''' @brief Reads and returns euler angles
            @details Euler angles are in degrees
        '''
        eul_angles = self.i2c.mem_read(euler_len,0x28,euler_init)
        eul_signed = struct.unpack('<hhh', eul_angles)
        eul_val = tuple(eul_int/900 for eul_int in eul_signed)
        return eul_val
        
    def read_angvel(self):
        ''' @brief Reads and returns euler angle velocities
            @details Velocities in degree/s
        '''
        omega = self.i2c.mem_read(gyr_len,0x28,gyr_init)
        omega_signed = struct.unpack('<hhh', omega)
        omega_val = tuple(-omega_int/900 for omega_int in omega_signed)
        return omega_val

