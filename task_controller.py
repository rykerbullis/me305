''' @file task_controller.py
    @brief 
    @details    
    @author Ryker Bullis
    @author Jack Barone
    @date December 4, 2021
'''

import utime
from ulab import numpy as np

K_T = 13.8*10**(-3)                 # [N-m/A]
R = 2.21                            # [Ohm]
V_DC = 12                           # [V]
K_duty = (100*R)/(4*K_T*V_DC)       # [%/N-m]


 
class Task_Controller:
    ''' @brief 
        @details    
    '''
    
    def __init__(self,position_vector,panel_vector,duty):
        ''' @brief 
        '''
        self.position_vector = position_vector
        self.panel_vector = panel_vector
        self.duty = duty
        
        
        
        
    def run(self):
        ''' @brief 
        '''

        self.x = self.position_vector[0].read()
        self.x_dot = self.position_vector[1].read()
        self.y = self.position_vector[2].read()
        self.y_dot = self.position_vector[3].read()
            
        self.theta_x = self.panel_vector[0].read()
        self.theta_dot_x = self.panel_vector[1].read()
        self.theta_y = self.panel_vector[2].read()
        self.theta_dot_y = self.panel_vector[3].read()
            
        # K = np.array([0.3, 0.2, 0.05, 0.02])
        
        # statevec_x = np.array([ [self.y], 
        #                         [self.theta_x],
        #                         [self.y_dot],
        #                         [self.theta_dot_x ] ])
                               
        # statevec_y = np.array([ [self.x], 
        #                         [self.theta_y],
        #                         [self.x_dot],
        #                         [self.theta_dot_y ] ])
        # T_x = np.dot(K,statevec_y)
        # T_y = np.dot(K,statevec_x)
        
        # X DIRECTION TUNING
        K1y =  3
        K2y = .06
        K3y =  .5
        K4y = .005
        
        # Y DIRECTION TUNING
        K1x = -5
        K2x = .04
        K3x = -3
        K4x = .0025
        
        T_y = K1y*self.x + K2y*self.theta_y + K3y*self.x_dot + K4y*self.theta_dot_y
        T_x = K1x*self.y + K2x*self.theta_x + K3x*self.y_dot + K4x*self.theta_dot_x
        
        D_x = K_duty*T_x
        D_y = K_duty*T_y
        if D_x >= 85 :
            D_x = 85
        if D_y >= 85:
            D_y = 85
            
        self.duty[0].write(D_x)
        self.duty[1].write(D_y)