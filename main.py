''' @file main.py
    @brief       Main file ball balancing platform
    @details     Runs tasks associated with c
    @author Jack Barone
    @author Ryker Bullis
    @date December 3, 2021                
'''
import pyb 
import shares
import DRV8847
import touchpanel
import task_motor 
import task_user
import task_TP
import task_imu
import task_controller

## Create pyb objects here  


## SHARES HERE
run_flag = shares.Share(False)
fault_flag = shares.Share(False)
duty = (shares.Share(0),shares.Share(0))

#touchpanel_out is state vector, [x_b,Vx_b,y_b,Vy_b]
position_vector = (shares.Share(),shares.Share(),shares.Share(),shares.Share())


## MOTOR STUFF ################################################################
## Instantiates motor driver by calling motor driver function
motor_drv = DRV8847.DRV8847(3,20000)
## Instantiates pin used for control of motor 1
IN1 = pyb.Pin(pyb.Pin.cpu.B4)
## Instantiates second pin used for control of motor 1
IN2 = pyb.Pin(pyb.Pin.cpu.B5)
## Instantiates pin used for control of motor 2
IN3 = pyb.Pin(pyb.Pin.cpu.B0)
## Instantiates second pin used for control of motor 2
IN4 = pyb.Pin(pyb.Pin.cpu.B1)

## Instantiate motor 1
motor_1 = motor_drv.motor(IN1, 1, IN2, 2)
## Instantiate motor 2
motor_2 = motor_drv.motor(IN3, 3, IN4, 4)
#######################################################################

## TOUCHPANEL STUFF ###################################################
pinA0 = pyb.Pin(pyb.Pin.cpu.A0)
pinA1 = pyb.Pin(pyb.Pin.cpu.A1)
pinA6 = pyb.Pin(pyb.Pin.cpu.A6)
pinA7 = pyb.Pin(pyb.Pin.cpu.A7)

xp = pyb.Pin(pinA7,mode=pyb.Pin.OUT_PP)
xm = pyb.Pin(pinA1,mode=pyb.Pin.OUT_PP)
yp = pyb.Pin(pinA0,mode=pyb.Pin.IN)
ym = pyb.Pin(pinA6,mode=pyb.Pin.IN)

touchpanel = touchpanel.TouchPanel(pinA0,pinA7, xp,xm,yp,ym)

## IMU STUFF  #########################################################

panel_vector = (shares.Share(),shares.Share(),shares.Share(),shares.Share())

if __name__ == '__main__': 
    
    task1 = task_TP.Task_TP(touchpanel,position_vector,run_flag)
    task2 = task_imu.Task_IMU(panel_vector)
    task3 = task_controller.Task_Controller(position_vector, panel_vector, duty)
    #task3 = task_user.Task_User(run_flag)
    task4 = task_motor.Task_Motor(motor_drv,duty[1], motor_1)
     
    task5 = task_motor.Task_Motor(motor_drv,duty[0], motor_2)
     
    while(True): 
        try: 

            task1.run()
            task2.run()
            task3.run()
            task4.run()
            task5.run()
            print(position_vector[0].read(),position_vector[2].read())
            #print(position_vector[0].read(),position_vector[1].read(),position_vector[2].read(),position_vector[3].read())
            #print(panel_vector[0].read(),panel_vector[1].read(),panel_vector[2].read(),panel_vector[3].read())
            #print(panel_vector[0].read(),panel_vector[1].read(),panel_vector[2].read(),panel_vector[3].read())
            #print(duty[1].read())
            #print(position_vector.get())
            #duty[1].write(60)
        except KeyboardInterrupt: 
            duty[1].write(0)
            duty[0].write(0)
            task4.run()
            task5.run()
            break 
    print('Program Terminating')
