'''@file                mainpage.py
   @brief               Page describing work done on Homework 2 and 3
   @details             Page describing how the equations of motion for the 
                        platform and ball system were obtained
                        
   @mainpage
                        
   @section wel         Welcome
                        This is a documentation repository for all work in ME305 - 
                        Introduction to Mechatronics for Ryker Bullis. Work includes
                        lab projects and homework assignments completed throughout
                        the Fall 2021 quarter. Please reference the navigation
                        browser on the left side of the page to view specific 
                        assignments. 
                        
                        The majority of assignments are focused on controlling
                        a ball-balancing platform, which balances a ball in two 
                        dimensions using motors to manipulate the orientation of 
                        the platform.
                        
                        \image html CENG_logo.png
                        
   @section source      Source Code
                        All source code is available here:
                        https://bitbucket.org/rykerbullis/me305

   \page 02 Homework0x02
   
   @section analysis    Overview
                        This analysis was performed to obtain the equations of motion for the 
                        ball on the balancing platform. Several assumptions were made to 
                        simplify the analysis, please refer to the images below for specifics.
                        All work was performed by hand to obtain the equations of motion in
                        matrix form, then MATLAB was utilized to linearize and simulate
                        the equations in Homework 0x03.
                        
   @section diagrams    Analysis
                        Please see the steps worked out by hand below to follow the analysis
                        of the system. The finalized matrix solution has the form
                        A x = B where x is a column vector with the state variables. 
                        
                        \image html HW21.jpg
                        \image html HW22.jpg
                        \image html HW23.jpg
                        \image html HW24.jpg                 
   
   \page 03 Homework0x03
   
   @section overview    Overview
                        This homework simulated the motion of the ball and platform
                        using the equations of motion obtained in Homework 0x02. 
                        The EOMs were first decoupled and linearized in MATLAB, 
                        then the system was simulated in Simulink. The results can be 
                        viewed on the plots below.
                        
   @section source      Source Code
                        The MATLAB and Simulink files used in this analysis can be viewed here:
                        https://bitbucket.org/rykerbullis/me305/src/master/HW2and3/
                        
   @section open        Open Loop Response
                        The open loop response does not use feedback to control
                        the system. The block diagram for the open loop system can 
                        be seen below:

                        \image html OL.png
                        
                        System response for initial condition x = 0 cm:
                        \image html HW3_1.jpg "Ball Position and Velocity"
                        \image html HW3_2.jpg "Platform Position and Velocity"
                        
                        Both of these system responses are to be expected for the 
                        given initial condition. The ball is initially at rest
                        perfectly centered on the platform. From a theoretical
                        perspective, there are no disturbances that can be introduced
                        which would drive any of the state variables away from 
                        zero value. Thus, the system response is expected. 
                        
                        System response for initial condition x = 5 cm:
                        \image html HW3_3.jpg "Ball Position and Velocity"
                        \image html HW3_4.jpg "Platform Position and Velocity"
                        
                        Both of these responses are also expected. The ball is initially
                        in an unbalanced position, and the open loop controller
                        tries to correct this. However, with a lack of feedback, 
                        we can see that the ball is not able to continue approaching
                        zero and eventually rolls off the edge of the platform. 
                        A similar response can be seen for the position and velocity
                        of the platform.
                        
   @section closed      Closed Loop Response
                        The closed loop response can be modeled using the block
                        diagram pictured below:
                        
                        \image html CL.jpg
                        
                        System response for initial condition x = 5 cm:
                        \image html HW3_5.jpg "Ball Position"
                        This response is to be expected. The initial displacement
                        of the ball from zero means that the controller attempts
                        to move the ball back to zero. The ball oscillates around 
                        a final zero value as the controller gets closer and closer
                        to its target value.
                        
                        \image html HW3_6.jpg "Ball Velocity"
                        This graph is logical because it is essentially the inverse
                        of the ball position graph. Mathematically and dynamically,
                        the ball will have the greatest velocity when the position 
                        of the ball is near zero, a phenomenon which holds true 
                        for this plot. The ball speed starts at an initial value
                        of zero, quickly increases to move the ball back and forth 
                        to the center position, and eventually comes to rest. 
                        
                        \image html HW3_7.jpg "Platform Position"
                        This image allows you to observe how the platform tilts 
                        the opposite direction of the ball in order to move it back
                        to center. Each of the directions it tilts correspond to 
                        opposite movement of the ball, which is what you would expect dynamically.
                        
                        \image html HW3_8.jpg "Platform Velocity"
                        This graph pictures the velocity of the platform, which 
                        is inversely proportional to the platform position, as 
                        was the case with the ball. 
                        
   \page 04 Lab0x04
   
   @section header      Lab0x04 - Closed Loop Speed Control
                        The purpose of this lab was to implement closed loop 
                        speed control for the motors. I implemented a proportional
                        controller and then tuned it for optimal performance. Eventually,
                        this speed controller will be used to control the motion 
                        of the platform while balancing a ball. 
                        
   @section doc4        Formal Documentation
                        Please visit the following link for formal documentation
                        of all files, classes, functions, and variables used in 
                        Lab 4:
                        https://rykerbullis.bitbucket.io/Lab0x04/
                        
                        Please see below for results of the lab. 
                        
   @section source      Source Code
                        Available here:
                        https://bitbucket.org/rykerbullis/me305/src/master/Lab4/
   
   @section diagrams    Task Diagrams and Tuning Plots
                        Task and state diagrams are used to model how different
                        files will interact, perform, and share data. Many of the
                        tasks run simultaneously and must perform their duties 
                        while not intefering with other tasks. Diagrams help to 
                        model the flow of information and structure the code.
   
                        See the following link for pdf documents of the state and task diagrams:
                        https://cpslo-my.sharepoint.com/:f:/g/personal/rbullis_calpoly_edu/EjEavnIYfnRJkHtu7U88-fkB-RLXx9aTk4OoJGsVe4E2eQ?e=o9GsuX

                        The motor controller performed well after tuning, as seen below:
                            
                        \image html Lab4BlockDiagram.png "Block diagram for the motor controller"
                        \image html Lab4Untuned1.png "First iteration of motor tuning"
                        \image html Lab4Untuned2.png "Second iteration of motor tuning"
                        \image html Lab4Untuned3.png "Third iteration of motor tuning"
                        \image html Lab4Untuned4.png "Fourth iteration of motor tuning"
                        \image html Lab4Tuned.png "Tuned motor controller"
                        
                        As can be seen from the tuned motor controller plot, the
                        proportional speed controller reaches the final target 
                        velocity and maintains it as directed. 

   @section sec_intro   Introduction
                        This lab implements a closed loop speed controller for a motor. 
                        It uses a proportional controller to set and control the speed 
                        of a motor. Implements motor and encoder tasks, as 
                        well as a user interface.

   @section sec_DRV8847 Motor Driver
                        Driver for the motors.
                        Please see DRV8847.DRV8847 for details.

   @section sec_enc     Encoder Driver
                        Driver for the motor encoders.
                        Please see encoder.Encoder for details.
                        
   @section sec_mot     Motor Task
                        Task which implements the motor driver.
                        Please see task_motor for details.

   @section sec_taskenc Encoder Task
                        Task which implements the encoder driver.
                        Please see task_enc for details.
   
   @section sec_cl      Closed Loop Controller
                        Implements proportional speed control for motor.
                        Please see closedloop for details.

   @section sec_share   Share
                        Allows for sharing of variables between tasks and files.
                        Please see shares.Share for details.
            
   @section sec_con     Task Controller
                        Implements the closed loop motor control in a task.
                        Please see task_controller for details.
                        
   @section sec_safety  Safety Task
                        Implements safety protection for motor faults.
                        Please see task_safety for details.
                        
   @section sec_user    User Task
                        Implements user interface, collecting and printing data.
                        Please see task_user for details.
   
   @author              Ryker Bullis
   @author              Jack Barone
   
   @date                November 29, 2021
'''