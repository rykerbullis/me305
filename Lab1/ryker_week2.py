# ALL imports should go at the top
import time

# The main program should go at the bottom after function definitions
if __name__ == '__main__':
    ## The state the finite state machine is about to run
    state = 0
    ## Number of iterations of the FSM
    runs = 0
    
    while(True):
        try:                        # If no keyboard interrupt, run FSM
            if (state == 0):
                # Run state 0 code
                print('In state 0')
                state = 1           # Transition to state 1
                
            elif (state == 1):
                # Run state 1 code
                print('In state 1')
                state = 2           # Transition to state 2
                
            elif (state == 2):
                # Run state 2 code
                print('In state 2')
                state = 3           # Transition to state 3
                
            elif (state == 3):
                # Run state 3 code
                print('In state 3')
                state = 4           # Transition to state 4  
            elif (state == 4):
                # Run state 4 code
                print('In state 4')
                state = 2           # Transition to state 2
            time.sleep(1)           # Slow down the FSM so we can read the console output
            runs += 1               # Increment run counter to track number of FSM iterations
            
        except KeyboardInterrupt:   # If keyboard interrupt, exit loop
            break
    print('Program terminated')