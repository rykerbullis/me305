'''@file                mainpage.py
   @brief               Page describing work done on Lab 1
                        
   @mainpage

   @section header1     Lab0x01 - Getting Started with Hardware
                        The purpose of this lab was to gain experience working 
                        with hardware in addition to Python. 
                        
   @section source1     Source code for Lab 1 is available here:
                        https://bitbucket.org/rykerbullis/me305/src/master/Lab1/
                        
   @section diagram1    State Transition Diagram and Video
                        The state transition diagram for Lab 1 can be seen below. 
                        It represents the different states which the script transitions
                        through as it completes the LED task.
                        
                        \image html Lab1State.PNG "State Transition Diagram"
                        
   @section video1      Video
                        A video demonstration of the code can be seen here: https://youtu.be/0CtW0Ivbhq0   
'''