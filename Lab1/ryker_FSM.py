''' @file ryker_FSM.py
    @brief Finite State Machine Code
    @details This file will be used to demonstrate one method for implementing
    a finite state machine in Python 

'''

# ALL imports should go at the top
import pyb
import time


def onButtonPressFCN(IRQ_src):
    global buttonState
    buttonState = True
   
# The main program should go at the bottom after function definitions
if __name__ == '__main__':
    ## The state the finite state machine is about to run
    state = 0
    ## Number of iterations of the FSM
    runs = 0
    buttonState = False
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13)

    ButtonInt = pyb.ExtInt(pinC13,mode=pyb.ExtInt.IRQ_FALLING,
                           pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
    
    while(True):
        try:                        # If no keyboard interrupt, run FSM
                if (state == 0):
                    # Run state 0 code
                    print('In state 0')
                    state = 1                  # Transition to state 1

                elif (state == 1):
                    # Run state 1 code
                    print('In state 1')
                    if (buttonState == True):
                        state = 2                  # Transition to state 2
                        buttonState = False
                    else:
                        pass
                    
                elif (state == 2):
                    # Run state 2 code
                    print('In state 2')
                    pinA5.high()
                    pyb.delay(500)
                    pinA5.low()
                    pyb.delay(500)
                    if (buttonState == True):
                        state = 3                  # Transition to state 3
                        buttonState = False
                    else:
                        pass
                    
                elif (state == 3):
                    # Run state 3 code
                    print('In state 3')
                    if (buttonState == True):
                        state = 4                  # Transition to state 4
                        buttonState = False
                    else:
                        pass
                    
                elif (state == 4):
                    # Run state 4 code
                    print('In state 4')
                    if (buttonState == True):
                        state = 2                  # Transition to state 2
                        buttonState = False
                    else:
                        pass               
                time.sleep(1)    
                runs += 1           # Increment run counter to track number of FSM iterations
                
        except KeyboardInterrupt:   # If keyboard interrupt, exit loop
            break
 
    print('Program terminated')