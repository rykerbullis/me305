''' @file main.py
    @brief Finite State Machine Code for Three Waveforms
    @details This file will be used to demonstrate one method for implementing
    a finite state machine in Python, and use it to advance through three 
    different states containing three waveforms used to light and LED.
    @author Ryker Bullis
    @author Jack Barone
    @date 10/4/2021
'''

import pyb
import math
import utime

def onButtonPressFCN(IRQ_src):
    '''@brief Callback function sets button state to true on button press
       @details A callback function that detects when there is a falling edge
       on PC13, the blue button on a Nucleo L476RG, and sets the button state
       to true in order to advance the finite state machine.
       @param IRQ_src Falling edge on PC13
       @return Interrupt request which sets button state to true
    '''
    ##@brief Button state TRUE or FALSE
    # @details Sets button state after it has been pressed
    #
    global buttonState
    buttonState = True

def resetTimer():
    '''@breif Resets the start timer
       @details Allows you to start the timer at the beginning of each cycle 
    '''
    
    ##@brief Start time
    # @details Computes the start time using utime
    #
    global startTime
    startTime = utime.ticks_ms()
    
def updateTimer():
    '''@brief Update the timer
       @details Updates the timer using the elapsed time
       @return The difference between the stop and start times
    '''
    ##@brief Stop time
    # @details Computes the stop time during timer update
    #
    stopTime = utime.ticks_ms()
    
    ##@brief Time difference
    # @details Computes the time difference between start and stop times
    #
    diff = utime.ticks_diff(stopTime,startTime)
    return diff
    
def updateSQW(currentTime):
    '''@brief Compute square waveform
       @details Computes a square waveform using utime for state 2
       @param currentTime Time from the update time function
       @return Square waveform
    '''
    return (currentTime%1 < .5)*100

def updateSTW(currentTime):
    '''@brief Compute sawtooth waveform
       @details Computes a sawtooth waveform using utime for state 3
       @param currentTime Time from the update time function
       @return Sawtooth waveform
    '''
    return (currentTime%1.0)*100

def updateSW(currentTime):
    '''@brief Compute sine waveform
       @details Computes a sine waveform using utime for state 4
       @param currentTime Time from the update time function
       @return Sine waveform
    '''
    return((50*math.sin(2*math.pi*currentTime/10))+50)
   
# The main program should go at the bottom after function definitions
if __name__ == '__main__':
    ## The state the finite state machine is about to run
    
    ##@brief state of the FSM
    # @details This keeps track of which state the FSM is in
    #
    state = 0
    ## Number of iterations of the FSM
    
    ##@brief state of button B1
    # @details this flags the input of the button on the Nucleo and triggers 
    # the FSM to tranisition
    #
    buttonState = False
    
    ##@brief creates pin object asocciated with C13 on the CPU
    # @details 
    #
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
    
    ##@brief creates call back function
    # @details Associate the callback function with the pin by setting up an external interrupt
    #
    
    ButtonInt = pyb.ExtInt(pinC13,mode=pyb.ExtInt.IRQ_FALLING,
                           pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
    
    ##@brief creates pin object associated with A5 on the CPU
    # @details 
    #
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
    
        
    while(True):
        try:                        # If no keyboard interrupt, run FSM
                if (state == 0):
                    # Run state 0 code
                    print('Hello, press button B1 on the Nucleo to cycle through LED patterns')
                    state = 1                  # Transition to state 1

                elif (state == 1):
                    # Run state 1 code
                    
                    if (buttonState == True):
                        state = 2                  # Transition to state 2
                        print('Displaying Square Wave')
                        buttonState = False
                        resetTimer()
                    else:
                        pass
                    
                elif (state == 2):
                    # Run state 2 code
                    
                    ##@brief pulls current time from update timer
                    # @details 
                    #
                    
                    currentTime = updateTimer()/1000
                    
                    ##@brief creates a pyb object for timer used in PWM
                    # @details 
                    #
                    
                    tim2 = pyb.Timer(2, freq = 20000)
                    
                    ##@brief specify pin of LED
                    # @details 
                    #
                    
                    t2ch1 = tim2.channel(1,pyb.Timer.PWM,pin=pinA5)
                    t2ch1.pulse_width_percent(updateSQW(currentTime))

                    if (buttonState == True):
                        state = 3                  # Transition to state 3
                        print('Displaying Sine Wave')
                        buttonState = False
                        resetTimer()
                    else:
                        pass
                    
                elif (state == 3):
                    # Run state 3 code
                    currentTime = updateTimer()/1000
                    tim2 = pyb.Timer(2,freq=20000)
                    t2ch1 = tim2.channel(1,pyb.Timer.PWM, pin=pinA5)
                    t2ch1.pulse_width_percent(updateSW(currentTime))
                    
                    if (buttonState == True):
                        state = 4                  # Transition to state 4
                        print('Displaying SawTooth Wave')
                        buttonState = False
                        resetTimer()
                    else:
                        pass
                    
                elif (state == 4):
                    # Run state 4 code
                    currentTime = updateTimer()/1000
                    tim2 = pyb.Timer(2,freq=20000)
                    t2ch1 = tim2.channel(1,pyb.Timer.PWM, pin=pinA5)
                    t2ch1.pulse_width_percent(updateSTW(currentTime))
                    
                    if (buttonState == True):
                        state = 2                  # Transition to state 2
                        print('Displaying Square Wave')
                        buttonState = False
                    else:
                        pass                 
                
        except KeyboardInterrupt:   # If keyboard interrupt, exit loop
            break
 
    print('Program terminated')